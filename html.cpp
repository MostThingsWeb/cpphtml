#include <libxml/xpath.h>
#include <libxml/tree.h>
#include <libxml/HTMLparser.h>
#include "HTTPHelper.h"
#include <libxml++/libxml++.h>

#include <iostream>
#include <string>

#include <memory>

using namespace xmlpp;

int main() {
    xmlInitParser();
    std::string re = HTTPHelper::GetURL("http://www.whatismyipaddress.com/");
    htmlDocPtr d = htmlReadDoc((xmlChar*)re.c_str(), NULL, NULL, HTML_PARSE_RECOVER | HTML_PARSE_NOERROR);

    Document* doc = new Document(d);

    std::shared_ptr<xmlpp::Element> root = std::shared_ptr<xmlpp::Element>(doc->get_root_node());
    std::string xpath = "//*[@id=\"section_left\"]/div[2]/text()";

    auto elements = root->find(xpath);

//    std::cout << doc->write_to_string() << std::endl;

    std::cout << "elements: " << elements.size() << std::endl;

    std::cout << static_cast<xmlpp::ContentNode*>(elements[1])->get_content() << std::endl;
    return 0;
}
