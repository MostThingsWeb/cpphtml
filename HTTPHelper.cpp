// Copyright (c) 2014 Chris Laplante (MostThingsWeb)

#include "HTTPHelper.h"

#include <boost/algorithm/string.hpp>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Infos.hpp>

#include <string>
#include <vector>
#include <map>
#include <sstream>

std::string HTTPHelper::GetURL(const std::string &url) {
	// Pass empty cookies
	CookieMap emptyCookies;
	return GetURL(url, emptyCookies);
}

std::string HTTPHelper::GetURL(const std::string &url, const CookieMap &inCookies) {
	// Ignore outCookies
	CookieMap outCookies;
	return GetURL(url, inCookies, outCookies);
}

std::string HTTPHelper::GetURL(const std::string &url, const CookieMap &inCookies, CookieMap &outCookies) {
	// Pass empty form data
	FormMap formData;
	return PerformRequest(url, inCookies, outCookies, formData);
}

std::string HTTPHelper::PostURL(const std::string &url, const CookieMap &inCookies, const FormMap &formData) {
	// Ignore outCookies
	CookieMap outCookies;
	return PerformRequest(url, inCookies, outCookies, formData);
}

std::string HTTPHelper::PerformRequest(const std::string &url, const CookieMap &inCookies, CookieMap &outCookies, const FormMap &formData) {
	curlpp::Easy request;

	// Specify the URL
	request.setOpt(curlpp::options::Url(url));

	// Specify some headers
	std::list<std::string> headers;
	headers.push_back(HEADER_ACCEPT);
	headers.push_back(HEADER_USER_AGENT);
	// Spoof a referer here if required
	//headers.push_back(HEADER_REFERER);

	// Initialize the cookie engine
	request.setOpt(new curlpp::options::CookieJar(""));

	// Any cookies to set?
	if (!inCookies.empty()) {
		// Append the cookie header to our master list of vectors
		headers.push_back(BuildCookieHeader(inCookies));
	}

	// Set our headers
	request.setOpt(new curlpp::options::HttpHeader(headers));
    request.setOpt(new curlpp::options::FollowLocation(true));

	// Any form data?
	if (!formData.empty()) {
		curlpp::Forms formParts;

		// Iterate over the form data
		for (FormMap::value_type entry : formData) {
            formParts.push_back(new curlpp::FormParts::Content(entry.first, entry.second));
		}

		request.setOpt(new curlpp::options::HttpPost(formParts));
	}

	// Configure curlpp to use stream
	std::ostringstream responseStream;
	curlpp::options::WriteStream streamWriter(&responseStream);
	request.setOpt(streamWriter);

	request.perform();

	// What tasty cookies did the server give us?
	std::list<std::string> outCookieStrings;
	// TODO: Fix memory leak at this line
    curlpp::infos::CookieList::get(request, outCookieStrings);

	// Parse those cookies
    for (std::string str : outCookieStrings) {
		ParseCookieString(str, outCookies);
	}

	return responseStream.str();
}

std::string HTTPHelper::BuildCookieHeader(const CookieMap &cookies) {
	std::string header = "Cookie: ";
	std::vector<std::string> cookieStrings;

	// Iterate over the cookie map
	for (CookieMap::value_type cookie : cookies) {
		cookieStrings.push_back(cookie.first + "=" + cookie.second);
	}

	header += boost::algorithm::join(cookieStrings, ";");

	return header;
}

void HTTPHelper::ParseCookieString(const std::string &str, CookieMap &cookies) {
	std::string part;
	std::istringstream strm(str);

	// Consume the first 5 parameters of the cookie - they're garbage
	for (int i = 0; i < 5; i++) {
		getline(strm, part, '\t');
	}

	// The last two parameters are the name and value
	std::string name, value;
	getline(strm, name, '\t');
	getline(strm, value, '\t');

	boost::algorithm::to_lower(name);

	cookies[name] = value;
}
