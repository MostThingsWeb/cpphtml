// Course.h - Course class implementation
// ncourses
// Written by Chris Laplante

#include "Course.h"
#include "HTTPHelper.h"
#include "Exceptions.h"
#include "DateTime.h"
#include "Section.h"

#include <boost/xpressive/xpressive.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/icl/interval_map.hpp>

#include <iostream>
#include <set>
#include <cstdlib>

// (Subject and Number const fields initialized via initializer list)
Course::Course(std::string subject, const std::string& number, boost::scoped_ptr<Session>& session): Subject(subject), Number(number) {
	boost::algorithm::to_upper(subject);
	boost::algorithm::trim(subject);

	// Compile a regular expression for parsing date-time information
	boost::xpressive::sregex dateTimeRegex = boost::xpressive::sregex::compile("^([MTWRF ]+) ([^-]+) - ([^-]+)$");

	// Build a request to handle the course fetch operation
	FormMap formData;
	formData["Semester"] = session->GetSemester();
	formData["CrseLoc"] = LONG_LOCATION;
	formData["CECrseLoc"] = LOCATION;
	formData["course_abbrev"] = subject;
	formData["course_num"] = number;

	std::string response = HTTPHelper::PostURL(SEARCH_URL, session->GetCookies(), formData);

	// Extract course sections
	xmlpp::DomParser doc;
	doc.parse_memory(HTTPHelper::CleanHTML(response));
	xmlpp::NodeSet tableData = doc.get_document()->get_root_node()->find("//*[contains(concat(' ', normalize-space(@class), ' '), ' course_details ')]/*[local-name() = 'td']");

	// Process each section
	for (unsigned int tableIndex = 0; tableIndex < tableData.size(); tableIndex += 11) {
		// Ignore sections with 0 seats maximum (we don't care how many of these seats are actually available)
		std::string seats = static_cast<xmlpp::ContentNode*>(tableData[tableIndex + 2]->find("*[local-name() = 'p']/text()")[0])->get_content();
		if (atoi(seats.substr(5, std::string::npos).c_str()) == 0) {
			continue;
		}

		// Extract schedule # and section #
		std::string scheduleNumber = static_cast<xmlpp::ContentNode*>(tableData[tableIndex]->find("*[local-name() = 'p']/text()")[0])->get_content();
		std::string sectionNumber = static_cast<xmlpp::ContentNode*>(tableData[tableIndex + 1]->find("*[local-name() = 'p']/text()")[0])->get_content();

		// Create a new section
		Section* section = new Section();
		Sections.push_back(section);

		section->ScheduleNumber = scheduleNumber;
		section->SectionNumber = sectionNumber;
		section->Course = GetName();

		// Extract the class meeting times and locations
		xmlpp::NodeSet dateTimes = tableData[tableIndex + 3]->find("*[local-name() = 'p']/text()");
		xmlpp::NodeSet roomNumbers = tableData[tableIndex + 4]->find("*[local-name() = 'p']/text()");
		xmlpp::NodeSet buildings = tableData[tableIndex + 4]->find("descendant-or-self::*[local-name() = 'a']");

		// Check for incomplete or missing building/date/time information
		if ((dateTimes.size() < 1) || !( (dateTimes.size() == roomNumbers.size()) && (roomNumbers.size() == buildings.size()) )) {
			if (IGNORE_INVALID_COURSES) {
				continue;
			}
			throw ResponseParseError("Course::Course(): The schedule for " + Subject + " " + Number + " is not fully defined.");
		}

		// One section can have different meeting times and places depending on the day
		for (unsigned int blockNum = 0; blockNum < dateTimes.size(); blockNum++) {
			// Parse the day/times
			boost::xpressive::smatch match;
			std::string dateTime = static_cast<xmlpp::ContentNode*>(dateTimes[blockNum])->get_content();
			regex_match(dateTime, match, dateTimeRegex);

			std::string days = match[1];
			boost::erase_all(days, " ");

			// Get building code and room number
			std::string buildingCode = static_cast<xmlpp::Element*>(buildings[blockNum])->get_attribute_value("href").substr(BUILDING_CODE_START, std::string::npos);
			std::string roomNumber = static_cast<xmlpp::ContentNode*>(buildings[blockNum]->get_parent()->find("text()")[0])->get_content();
			boost::algorithm::trim(roomNumber);

			// Build schedule
			for (unsigned int dayNum = 0; dayNum < days.size(); dayNum++) {
				// Parse start and end time
				unsigned int startTime = DateTime::CreateTime(days[dayNum], match[2]);
				unsigned int endTime = DateTime::CreateTime(days[dayNum], match[3]);

				// Create an interval
				boost::icl::interval<unsigned int>::type thisInterval(startTime, endTime);
				std::set<std::string> locationSet;
				locationSet.insert(roomNumber + "|" + buildingCode);

				// Add the interval to the schedule for this section
				section->Schedule.add(make_pair(thisInterval, locationSet));
			}
		}
	}

	if (Sections.size() < 1) {
		throw ResponseParseError("Course::Course(): No course sections were found.");
	}
}

const boost::ptr_vector<Section>& Course::GetSections() const {
	return Sections;
}

const Section* Course::GetSection(unsigned int index) const {
	return &Sections[index];
}

int Course::GetSectionCount() const {
	return Sections.size();
}

std::string Course::GetName() const {
	return Subject + " " + Number;
}
