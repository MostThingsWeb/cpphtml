#include <tidy/tidy.h>
#include <curlpp/cURLpp.hpp>
#include <tidy/buffio.h>
#include <libxml++/libxml++.h>
#include "HTTPHelper.h"
#include <iostream>

bool username_available(const std::string& username) {
    std::string url = "http://www.reddit.com/api/username_available.json?user=";
    url = url + username;
    std::string response = HTTPHelper::GetURL(url);
    return response == "true";
}

std::string get_ip_address() {
    std::string url = "https://www.whatismyip.com/";
    std::string response = HTTPHelper::GetURL(url);

    xmlpp::DomParser doc;
    doc.parse_memory(HTTPHelper::CleanHTML(response));
//    std::cout << response << std::endl;
    std::cout << HTTPHelper::CleanHTML(response) << std::endl;
    xmlpp::Element* root = doc.get_document()->get_root_node();
    std::string xpath = "descendant-or-self::*[@id = 'home']";

    auto elements = root->find(xpath);

    std::cout << "elements: " << elements.size() << std::endl;

    std::cout << static_cast<xmlpp::ContentNode*>(elements[0])->get_content() << std::endl;
}

void example1() {
    std::cout << "=== Example 1: Reddit user name checker ===" << std::endl;
    std::cout << "Checking if usernames are available:" << std::endl;
    std::vector<std::string> names = { "test", "fnewlg23g2g", "Soooperman", "matt", "mfj" };

    for (std::string name : names) {
        std::cout << " + " << name << " => " << (username_available(name) ? "Available!" : "Taken") << std::endl;
    }
}

int main() {
    curlpp::initialize();
    get_ip_address();

    example1();

    CookieMap in;
    CookieMap out;
    std::string g = HTTPHelper::GetURL("http://www.reddit.com", in, out);

    std::cout << "Got " << out.size() << " cookies" << std::endl;
    for (auto cookie : out) {
        std::cout << "+++ " << cookie.second << std::endl;
    }

    //xmlpp::DomParser doc;

    // 'response' contains your HTML
    /*
    doc.parse_memory(CleanHTML(response));

    xmlpp::Document* document = doc.get_document();
    xmlpp::Element* root = document->get_root_node();

    xmlpp::NodeSet elemns = root->find("descendant-or-self::*[@id = 'some_id']");
    std::cout << elemns[0]->get_line() << std::endl;
    std::cout << elemns.size() << std::endl;
    */

    curlpp::terminate();
}

