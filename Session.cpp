// Session.cpp - Schedule of courses session class implementation
// Written by Chris Laplante

#include "HTTPHelper.h"
#include "Session.h"
#include "Exceptions.h"

#include <boost/algorithm/string.hpp>
#include <libxml++/libxml++.h>

Session::Session() {
	// Initialize a new SOC session

	// Empty map of cookies going in
	CookieMap inCookies;
	// This will store the cookies the server sends us
	CookieMap outCookies;

	// Perform a request to the SOC home page
	std::string response = HTTPHelper::GetURL(SOC_HOME, inCookies, outCookies);

	// See if we got our session variables
	CookieMap::iterator cfid = outCookies.find("cfid");
	CookieMap::iterator cftoken = outCookies.find("cftoken");

	if ((cfid == outCookies.end()) || (cftoken == outCookies.end())) {
		throw InvalidSessionError("Session::Session(): The sesson ID and token were not set by the server.");
	}

	SessionCookies["CFID"] = cfid->second;
	SessionCookies["CFTOKEN"] = cftoken->second;

	// Parse the session response to retreive a list of sessions the website is currently supporting
	xmlpp::DomParser doc;
	doc.parse_memory(HTTPHelper::CleanHTML(response));

	// Extract semesters
	for (xmlpp::Node* semesterOption : doc.get_document()->get_root_node()->find("//*[@id='semester']/*[local-name() = 'option']/text()")) {
		// Get the value
		std::string value = static_cast<xmlpp::ContentNode*>(semesterOption)->get_content();
		Semesters.push_back(value);
	}

	if (Semesters.size() < 1) {
		throw ResponseParseError("Session::Session(): No active semesters were found.");
	}

	// Initialize session campus and location
	SessionCookies["SOC_CRSELOCCOOKIE"] = CAMPUS;
	SessionCookies["SOC_CECRSELOCCOOKIE"] = LOCATION;
	SessionCookies["SOC_ADVANCEDCRSELOCCOOKIE"] = CAMPUS;
	SessionCookies["SOC_ADVANCEDCECRSELOCCOOKIE"] = LOCATION;

	// Set initial semester
	SetSemester(0);
}

CookieMap Session::GetCookies() const {
	return SessionCookies;
}

const std::vector<std::string>& Session::GetSemesters() const {
	return Semesters;
}

void Session::ValidateSessionIndex(unsigned int i, const std::string& callingFunction) const {
	if (i > (Semesters.size() - 1)) {
		throw OutOfRangeError("Session::" + callingFunction + ": The semester index is out of range.");
	}
}

void Session::SetSemester(unsigned int i) {
	ValidateSessionIndex(i, "SetSemester()");
	Semester = i;

	// Escape the session and set its cookie
	std::string semester = Semesters[i];
	boost::replace_all(semester, " ", "%20");

	SessionCookies["SOC_SEMCOOKIE"] = semester;
	SessionCookies["SOC_ADVANCEDSEMCOOKIE"] = semester;
}

std::string Session::GetSemester() const {
	ValidateSessionIndex(Semester, "GetSemester()");
	return Semesters[Semester];
}
