// Copyright (c) 2014 Chris Laplante (MostThingsWeb)

/*
 * HTTPHelper provides a simpler wrapper around the curlpp and tidy libraries. It supports HTTP GET and POST operations (with or without cookies and form data).
 * Using Tidy, it also supports converting HTML to XML.
 */

#pragma once

#include <string>
#include <map>

// HTTP headers
#define HEADER_ACCEPT "Accept:text/html,application/xhtml+xml,application/xml"
#define HEADER_USER_AGENT "User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.70 Safari/537.17"
//#define HEADER_REFERER "Referer:http://www.google.com"

// A map for storing cookies
typedef std::map<std::string, std::string> CookieMap;

// A map for storing form data
typedef std::map<std::string, std::string> FormMap;

class HTTPHelper {
	public:
		// Perform a GET request on the specified URL and return the response body.
		static std::string GetURL(const std::string &url);

		// Perform a GET request on the specified URL, with the given name/value map of cookies, and return
		// the response body.
		static std::string GetURL(const std::string &url, const CookieMap &inCookies);

		// Perform a POST request on the specified URL, with the given name/value map of cookies (inCookies) and form data.
		static std::string PostURL(const std::string &url, const CookieMap &inCookies, const FormMap &formData);

		// Perform a GET request on the specified URL, with the given name/value map of cookies (inCookies). Once the response
		// is complete, populate outCookies with the cookies received from the server. Finally, return the response body.
		static std::string GetURL(const std::string &url, const CookieMap &inCookies, CookieMap &outCookies);

	private:
		// Perform a POST request (or GET if formData is empty) on the specified URL, with the given name/value map of cookies (inCookies) and form data.
		// Once the response is complete, populate outCookies with the cookies received from the server. Finally, return the response body.
		static std::string PerformRequest(const std::string &url, const CookieMap &inCookies, CookieMap &outCookies, const FormMap &formData);

		// Populate the given name/value map of cookies given a tab-delimited cookie string.
		static void ParseCookieString(const std::string &str, CookieMap &cookies);

		// Create an HTTP cookie header given a name/value map of cookies.
		static std::string BuildCookieHeader(const CookieMap &cookies);
};
